package society.property;

public enum BorderType {
    REGULAR, TOROIDAL, INFINITE;
}
