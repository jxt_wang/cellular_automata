package society;

import cell.Cell;
import cell.CellShape;

import java.util.List;

/**
 * Created by Justin Wang
 */

public class SpreadingFireSociety extends Society {

    public SpreadingFireSociety(int row, int col, List<Cell> cellList, CellShape cellShape) {
        super(row, col, cellList, cellShape);
    }

}
