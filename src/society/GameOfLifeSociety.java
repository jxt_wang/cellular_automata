package society;

import cell.Cell;
import cell.CellShape;

import java.util.List;

/**
 * Created by Justin Wang
 */

public class GameOfLifeSociety extends Society {

    public GameOfLifeSociety(int row, int col, List<Cell> cellList, CellShape cellShape) {
        super(row, col, cellList, cellShape);
        
    }

}
