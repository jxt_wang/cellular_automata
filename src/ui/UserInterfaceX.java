package ui;

import cell.*;
import cell.Cell;
import cell.state.GameOfLifeState;
import cell.state.SegregationState;
import cell.state.SpreadingFireState;
import cell.state.WatorState;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import society.Society;
import society.property.SocietySetting;
import society.property.SocietyType;
import xml.XMLParser;

import java.io.File;
import java.util.EnumMap;
import java.util.ResourceBundle;

/**
 * Created by Justin Wang
 */

public class UserInterfaceX {

    private final String UIPropertiesLocation = "ui/property/UITags";
    private ResourceBundle UIProperties = ResourceBundle.getBundle(UIPropertiesLocation);

    private Animator mainAnimator;

    private BorderPane borderPane;
    private Scene primaryScene;

    private ToolBar topPane;
    private ToolBar bottomPane;
    private VBox menuPane;
    private ScrollPane societyPane;

    private Text titleText;

    private Button selectFile;
    private Button resetSociety;
    private ComboBox simulationOptions;
    private ComboBox changeShape;

    private ToggleButton startStopButton;
    private Slider animationSlider;
    private Slider cellSlider;

    private TabPane settingsPaneA;
    private TabPane settingsPaneB;
    private Tab settingsTab;
    private Tab graphTab;

    private HBox mainBox;

    private Button nextStep;

    private EnumMap<SocietyType, String> fileMap;

    private XMLParser xmlParser;

    public UserInterfaceX(){

        initFilePaths();
        initBorderPane();

    }

    private void initFilePaths(){

        fileMap = new EnumMap<SocietyType, String>(SocietyType.class){{
            put(SocietyType.SPREADING_FIRE, "resource/SF_A.xml");
            put(SocietyType.WATOR, "resource/WT_A.xml");
            put(SocietyType.SEGREGATION, "resource/SEG_A.xml");
            put(SocietyType.GAME_OF_LIFE, "resource/GOL_A.xml");
        }};

    }

    private SocietySetting getSettings(String filePath) throws Exception{
        this.xmlParser = new XMLParser(filePath);
        return xmlParser.getSocietySetting();
    }

    private Society createSociety(String filePath, CellShape cellShape) throws Exception {
        SocietySetting societySetting = this.getSettings(filePath);
        Society newSociety = societySetting.getSocietyType().createSociety(societySetting.getRow(), societySetting.getCol(), societySetting.getCellList(), cellShape);
        setCellScale(4.0, newSociety);
        return newSociety;
    }

    private void initBorderPane() {

        borderPane = new BorderPane(){{
            setPrefWidth(1200);
            setPrefHeight(800);
        }};

        primaryScene = new Scene(borderPane);

        initTopPane();
        initBottomPane();
        initMenuPane();
        initSocietyPane();

    }

    private void initTopPane() {

        topPane = new ToolBar(){{
            setBlendMode(BlendMode.SRC_ATOP);
            setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            prefWidth(1200);
        }};

        Insets xPadding = new Insets(10, 10, 10, 10);

        titleText = new Text("Cell Society | Computer Science 308"){{
            setFont(Font.font("Calibri Light", 24));
        }};

        HBox LeftBox = new HBox(){{
            setPadding(xPadding);
            getChildren().add(titleText);
            setPrefWidth(200);
        }};

        selectFile = new Button(){{
            setText("Select File");
            setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        createFileChooser();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }};

        simulationOptions = listSimulationOptions();

        HBox RightBox = new HBox(){{

            setAlignment(Pos.CENTER_RIGHT);
            getChildren().addAll(selectFile, simulationOptions);
            setMargin(selectFile, xPadding);
            setMargin(simulationOptions, xPadding);

            setPrefWidth(980);

        }};

        topPane.getItems().addAll(
            LeftBox, RightBox
        );

        borderPane.setTop(topPane);

    }

    private void createFileChooser() throws Exception {

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extentionFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extentionFilter);

        String userDirectoryString = System.getProperty("user.dir");
        File userDirectory = new File(userDirectoryString);
        fileChooser.setInitialDirectory(userDirectory);

        fileChooser.setTitle("Open Simulation File");
        File selectedFile = fileChooser.showOpenDialog(null);

        if(selectedFile != null){

            String filePath = selectedFile.getPath();
            SocietySetting socSetting = getSettings(filePath);
            Society xSociety = createSociety(selectedFile.getPath(), CellShape.SQUARE);
            mainAnimator = new Animator(socSetting.getSocietyType(), xSociety);

            changeShape.getSelectionModel().select(xSociety.getSocietyGrid().getCellShape().toString());

            enableBottom();

            renderSociety(mainAnimator.getSociety());
            positionAnimatorChart();
            //mainAnimator.initialize();
            setSettings(socSetting.getSocietyType());

            startStopButton.setSelected(false);
        }

    }

    private void initSocietyPane() {

        societyPane = new ScrollPane(){{
            setPannable(true);
        }};
        BorderPane.setAlignment(societyPane, Pos.CENTER);
        borderPane.setCenter(societyPane);

    }

    private void initMenuPane() {

        menuPane = new VBox(){{
            setPrefWidth(400);
        }};
        BorderPane.setAlignment(menuPane, Pos.CENTER);
        borderPane.setLeft(menuPane);

        settingsTab = new Tab(){{
            setText("Settings");
            setClosable(false);
        }};

        graphTab = new Tab(){{
            setText("Graph");
            setClosable(false);
        }};

        settingsPaneA = new TabPane(){{
           setPrefHeight(340);
           getTabs().add(settingsTab);
        }};

        settingsPaneB = new TabPane(){{
            setPrefHeight(340);
            setPrefWidth(400);
            getTabs().add(graphTab);
        }};

        menuPane.getChildren().addAll(settingsPaneA, settingsPaneB);

    }

    private void initBottomPane() {

        Insets xPadding = new Insets(30, 30, 30, 30);

        bottomPane = new ToolBar(){{
            setBlendMode(BlendMode.SRC_ATOP);
            setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            prefWidth(1200);
            prefHeight(100);
        }};

        animationSlider = new Slider(){{
            setId(UIProperties.getString("AnimationRateId"));
        }};

        nextStep = new Button(){{
            setText("Next Turn");
            setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    mainAnimator.nextStep();
                }
            });
        }};

        VBox animationSliderBox = new VBox(){{
            getChildren().addAll(new Text("Animation Rate"), animationSlider);
            setPadding(xPadding);
            setSpacing(10);
            setAlignment(Pos.CENTER);
        }};

        cellSlider = new Slider();

        VBox cellSliderBox = new VBox(){{
            getChildren().addAll(new Text("Cell Size"), cellSlider);
            setPadding(xPadding);
            setSpacing(10);
            setAlignment(Pos.CENTER);
        }};

        changeShape = listShapeOptions();

        this.initSlider(animationSlider);

        this.initSizeSlider(cellSlider);

        animationSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                mainAnimator.changeRate(newValue.doubleValue());
            }
        });

        cellSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                mainAnimator.getSociety().setCellScale(newValue.doubleValue());
            }
        });

        startStopButton = new ToggleButton(){{
            setText("Start | Stop");
            selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if(!oldValue){
                        start();
                    }
                    else{
                        stop();
                    }
                }

            });
        }};

        resetSociety = new Button(){{
            setText("Reset Society");
            setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    try {
                        reset();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }};

        mainBox = new HBox(){{
            getChildren().addAll(startStopButton, nextStep, changeShape, resetSociety, animationSliderBox, cellSliderBox);
            setPrefWidth(1185);
            setAlignment(Pos.CENTER);
            setMargin(startStopButton, xPadding);
            setMargin(animationSlider, xPadding);
            setMargin(changeShape, xPadding);
            setMargin(resetSociety, xPadding);
            setMargin(cellSlider, xPadding);
            setMargin(nextStep, xPadding);
            setDisable(true);
        }};

        bottomPane.getItems().add(mainBox);

        borderPane.setBottom(bottomPane);

    }

    private void initSizeSlider(Slider cellSlider) {
        cellSlider.setMin(1);
        cellSlider.setMax(10);
    }

    private void reset() throws Exception{
        String xShape = changeShape.getValue().toString();
        mainAnimator = new Animator(mainAnimator.getSocietyType(), createSociety(xmlParser.getFilePath(), CellShape.valueOf(xShape)));
        mainAnimator.getSociety().setCellScale(cellSlider.getValue());
        cellSlider.setValue(mainAnimator.getSociety().getCellScale());
        changeShape.getSelectionModel().select(mainAnimator.getSociety().getSocietyGrid().getCellShape().toString());
        renderSociety(mainAnimator.getSociety());
        positionAnimatorChart();
        startStopButton.setSelected(false);
    }

    private void initSlider(Slider xSlider){
        xSlider.setMin(0);
        xSlider.setMax(6);
        xSlider.setValue((xSlider.getMax()-xSlider.getMin())/2);
    }

    /**
     * List all simulation options
     */
    private ComboBox listSimulationOptions() {

        ComboBox comboBox = new ComboBox();
        comboBox.getItems().addAll(UIProperties.getString("FireSimTitle"), UIProperties.getString("PredatorSimTitle"),
                UIProperties.getString("SegregationSimTitle"), UIProperties.getString("GameofLifeSimTitle"));

        initSimOptions(comboBox);

        return comboBox;
    }

    private ComboBox listShapeOptions() {

        ComboBox comboBox = new ComboBox();
        comboBox.getItems().addAll(UIProperties.getString("SQUARE"), UIProperties.getString("HEXAGON_POINT"),
                UIProperties.getString("HEXAGON_EDGE"), UIProperties.getString("TRIANGLE"));

        comboBox.setPromptText(UIProperties.getString("ComboBoxPromptShape"));
        comboBox.valueProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {

                CellShape cellShape = CellShape.valueOf(arg2.toString());

                try {
                    setCellShape(cellShape, mainAnimator.getSociety());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        return comboBox;
    }

    private void setCellShape(CellShape cellShape, Society xSociety) {
        xSociety.setCellShape(cellShape);
    }

    private void initSimOptions(ComboBox comboBox) {

        comboBox.setPromptText(UIProperties.getString("ComboBoxPrompt"));
        comboBox.valueProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {

                SocietyType sType = SocietyType.fromString(arg2.toString());

                try {
                    mainAnimator = new Animator(sType, createSociety(fileMap.get(sType), CellShape.SQUARE));
                    changeShape.getSelectionModel().select(mainAnimator.getSociety().getSocietyGrid().getCellShape().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

                enableBottom();

                renderSociety(mainAnimator.getSociety());
                positionAnimatorChart();
                startStopButton.setSelected(false);
                setSettings(sType);
            }

        });

    }

    public void enableBottom(){
        mainBox.setDisable(false);
    }

    /**
     * Sets all the cells into a gridpane to be displayed
     * @param Society calls the grid upon which the cells are built
     */
    private void renderSociety(Society xSociety){

        Group viewSociety = new Group();
        xSociety.getSocietyGrid().renderCells();
        for(Cell xCell : xSociety.getSocietyGrid().getCellMap().values()){
            viewSociety.getChildren().add(xCell);
            xCell.setOnMouseClicked(new EventHandler<MouseEvent>(){
                @Override
                public void handle(MouseEvent arg0){
                    xCell.setCurrentState(xCell.getStateSwapMap().get(xCell.getCurrentState()));
                }
            });
        }

        double tempScale = xSociety.getCellScale();

        while(viewSociety.getBoundsInParent().getWidth() > (societyPane.getWidth())){
            setCellScale(tempScale-=0.1, xSociety);
        }

        cellSlider.setValue(mainAnimator.getSociety().getCellScale());

        StackPane groupHolder = new StackPane(viewSociety);

        societyPane.setContent(groupHolder);

        societyPane.setFitToWidth(true);
        societyPane.setFitToHeight(true);

    }

    private void start(){
        if(mainAnimator.isInitialized()){
            mainAnimator.play();
        }
        else{
            mainAnimator.initialize();
            mainAnimator.changeRate(animationSlider.getValue());
        }
        nextStep.setDisable(true);
    }

    private void stop(){
        if(mainAnimator.isInitialized()) {
            mainAnimator.stop();
        }
        nextStep.setDisable(false);
    }

    public Scene getPrimaryScene() {
        return primaryScene;
    }

    /**
     * Positions the chart
     */
    private void positionAnimatorChart() {
        graphTab.setContent(mainAnimator.getLineChart());
    }

    private void setCellScale(double xFactor, Society xSociety){
        xSociety.setCellScale(xFactor);
    }

    private void swapSettings(VBox vBox){
        settingsTab.setContent(vBox);
    }

    private void setSettings(SocietyType societyType){

        VBox societySettingsBox = new VBox();

        switch (societyType){
            case SEGREGATION:
                societySettingsBox = createSegregationSettings();
                break;
            case SPREADING_FIRE:
                societySettingsBox = createFireSettings();
                break;
            case GAME_OF_LIFE:
                societySettingsBox = createGOLSettings();
                break;
            case WATOR:
                societySettingsBox = createWatorSettings();
                break;
        }

        swapSettings(societySettingsBox);

    }

    private VBox createSegregationSettings() {

        Label rateLabel = new Label();

        Slider satisfactionRate = new Slider(){{
            setMin(0);
            setMax(1);
            setValue(SegregationState.AGENT_X.getSatisfactionRate());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(mainAnimator.getSocietyType() == SocietyType.SEGREGATION){
                        SegregationState.AGENT_X.setSatisfactionRate(newValue.doubleValue());
                        rateLabel.setText(String.valueOf(getValue()*100));
                    }
                }
            });
        }};

        rateLabel.setText(String.valueOf(satisfactionRate.getValue()*100));

        Label rateLabelB = new Label();

        Slider satisfactionRateB = new Slider(){{
            setMin(0);
            setMax(1);
            setValue(SegregationState.AGENT_O.getSatisfactionRate());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(mainAnimator.getSocietyType() == SocietyType.SEGREGATION){
                        SegregationState.AGENT_O.setSatisfactionRate(newValue.doubleValue());
                        rateLabelB.setText(String.valueOf(getValue()*100));
                    }
                }
            });
        }};

        rateLabelB.setText(String.valueOf(satisfactionRateB.getValue()*100));

        VBox tempBox = new VBox(){{
            getChildren().addAll(new Text("Satisfaction Rate For X"), satisfactionRate, rateLabel,
                    new Text("Satisfaction Rate For O"), satisfactionRateB, rateLabelB);
            setSpacing(10);
            setAlignment(Pos.CENTER);
            setPadding(new Insets(75, 75, 75, 75));
        }};

        return tempBox;

    }

    private VBox createFireSettings() {

        Label rateLabel = new Label();

        Slider catchRate = new Slider(){{
            setMin(0);
            setMax(1);
            setValue(SpreadingFireState.TREE.getCatchProb());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(mainAnimator.getSocietyType() == SocietyType.SPREADING_FIRE){
                        SpreadingFireState.TREE.setCatchProb(newValue.doubleValue());
                        rateLabel.setText(String.valueOf(getValue()*100));
                    }
                }
            });
        }};

        rateLabel.setText(String.valueOf(catchRate.getValue()*100));

        VBox tempBox = new VBox(){{
            getChildren().addAll(new Text("Probability of Catching Fire"), catchRate, rateLabel);
            setSpacing(10);
            setAlignment(Pos.CENTER);
            setPadding(new Insets(75, 75, 75, 75));
        }};

        return tempBox;
    }

    private Slider aliveMaxRate, aliveMinRate;

    private VBox createGOLSettings() {

        Label rateLabel = new Label();

        Slider deadParameter = new Slider(){{
            setMin(1);
            setMax(mainAnimator.getSociety().getSocietyGrid().getCellShape().getNumSides());
            setMajorTickUnit(1);
            setShowTickMarks(true);
            setMinorTickCount(0);
            setSnapToTicks(true);
            setValue(GameOfLifeState.DEAD.getMax());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(mainAnimator.getSocietyType() == SocietyType.GAME_OF_LIFE){
                        GameOfLifeState.DEAD.setMax(newValue.intValue());
                        rateLabel.setText(String.valueOf((int)getValue()));
                    }
                }
            });
        }};

        rateLabel.setText(String.valueOf((int)deadParameter.getValue()));

        Label rateLabelB = new Label();

        aliveMaxRate = new Slider(){{
            setMin(1);
            setMax(mainAnimator.getSociety().getSocietyGrid().getCellShape().getNumSides());
            setMajorTickUnit(1);
            setShowTickMarks(true);
            setMinorTickCount(0);
            setSnapToTicks(true);
            setValue(GameOfLifeState.ALIVE.getMax());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(aliveMinRate.getValue() <= newValue.intValue()){
                        GameOfLifeState.ALIVE.setMax(newValue.intValue());
                        rateLabelB.setText(String.valueOf((int)getValue()));
                    }
                    else{
                        setValue(aliveMinRate.getValue());
                    }
                }
            });
        }};

        rateLabelB.setText(String.valueOf((int)aliveMaxRate.getValue()));

        Label rateLabelC = new Label();

        aliveMinRate = new Slider(){{
            setMin(1);
            setMax(mainAnimator.getSociety().getSocietyGrid().getCellShape().getNumSides());
            setMajorTickUnit(1);
            setShowTickMarks(true);
            setMinorTickCount(0);
            setSnapToTicks(true);
            setValue(GameOfLifeState.ALIVE.getMin());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(aliveMaxRate.getValue() >= newValue.intValue()){
                        GameOfLifeState.ALIVE.setMin(newValue.intValue());
                        rateLabelC.setText(String.valueOf((int)getValue()));
                    }
                    else{
                        setValue(aliveMaxRate.getValue());
                    }
                }
            });
        }};

        rateLabelC.setText(String.valueOf((int)aliveMinRate.getValue()));

        VBox tempBox = new VBox(){{
            getChildren().addAll(new Text("Dead Cell Requirement (# Neighbors)"), deadParameter, rateLabel,
                    new Text("Alive Cell Max (# Neighbors)"), aliveMaxRate, rateLabelB,
                    new Text("Alive Cell Min (# Neighbors)"), aliveMinRate, rateLabelC);
            setSpacing(10);
            setAlignment(Pos.CENTER);
            setPadding(new Insets(10, 75, 0, 75));
        }};

        return tempBox;

    }

    private VBox createWatorSettings() {

        Label rateLabel = new Label();

        Slider predatorBreedRate = new Slider(){{
            setMin(1);
            setMax(30);
            setMajorTickUnit(1);
            setShowTickMarks(true);
            setMinorTickCount(0);
            setSnapToTicks(true);
            setValue(WatorState.PREDATOR.getBreedTime());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(mainAnimator.getSocietyType() == SocietyType.WATOR){
                        WatorState.PREDATOR.setBreedTime(newValue.intValue());
                        rateLabel.setText(String.valueOf((int)getValue()));
                    }
                }
            });
        }};

        rateLabel.setText(String.valueOf((int)predatorBreedRate.getValue()));

        Label rateLabelB = new Label();

        Slider preyBreedRate = new Slider(){{
            setMin(1);
            setMax(30);
            setMajorTickUnit(1);
            setShowTickMarks(true);
            setMinorTickCount(0);
            setSnapToTicks(true);
            setValue(WatorState.PREY.getBreedTime());
            valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(mainAnimator.getSocietyType() == SocietyType.WATOR){
                        WatorState.PREY.setBreedTime(newValue.intValue());
                        rateLabelB.setText(String.valueOf((int)getValue()));
                    }
                }
            });
        }};

        rateLabelB.setText(String.valueOf((int)preyBreedRate.getValue()));

        VBox tempBox = new VBox(){{
            getChildren().addAll(new Text("Predator Breed Time (Turns)"), predatorBreedRate, rateLabel,
                    new Text("Prey Breed Time (Turns)"), preyBreedRate, rateLabelB);
            setSpacing(10);
            setAlignment(Pos.CENTER);
            setPadding(new Insets(75, 75, 75, 75));
        }};

        return tempBox;

    }


}