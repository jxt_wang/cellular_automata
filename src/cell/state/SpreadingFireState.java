// This entire file is part of my masterpiece
// Justin T Wang

/**
 * This is one example of a State enum implementing the State interface.
 *
 * Since the class is forced to override the methods outlined in State, it is now able to extend a feature required by all
 * simulations to better accommodate for the rules of its own.
 *
 * The extension of the neighborUpdate method in the specific *Tree* enum constant showcases the benefits of implementing
 * an inheritance pattern - guaranteeing flexibility while maintaining individuality.
 *
 * The setter/getter method names are descriptive, with the code itself limited to the fewest lines possible.
 *
 * The main benefit of using an enum, however, resides in its static behavior. Since its attributes effectively extend across
 * all elements that implement it, changing society-wide rules becomes a one-step process. For instance, instead of having to change
 * the probability of catching fire for every individual cell, a single call to the SpreadingFireState enum will suffice.
 * This adjustment will then be reflected across all of the cells that contain the SpreadingFireState.
 */

package cell.state;

import cell.state.property.StateColor;
import cell.state.property.StateRules;
import javafx.scene.paint.Color;

import java.util.List;
import java.util.Random;

public enum SpreadingFireState implements State{

    EMPTY(StateColor.SF_EMPTY.getColor()), BURNING(StateColor.SF_BURNING.getColor()),
    TREE(StateColor.SF_TREE.getColor()){

        /**
         * Special neighborUpdate method for tree.
         * @param neighborStates list of the <code>States</code> surrounding the current <code>State</code>
         * @return the next <code>State</code>
         */
        @Override
        public State neighborUpdate(List<State> neighborStates) {

            double randDouble = this.getRandGen().nextDouble();
            return (neighborStates.contains(BURNING) && (randDouble <= this.getCatchProb()))? BURNING : TREE;

        }

    };

    private double catchProb;
    private Random randGen;
    private Color stateColor;

    SpreadingFireState(Color stateColor){

        this.catchProb = StateRules.SF_PROBABILITY.getValue();
        this.randGen = new Random();
        this.setColor(stateColor);

    }

    /*Primary Functions*/

    /**
     * Checks neighboring <code>states</code> and returns the next <code>State</code>.
     * @param neighborStates list of the <code>states</code> surrounding the current <code>State</code>
     * @param probCatch probability of a <code>Tree</code> catching fire (value between 0.0 and 1.0)
     * @return the next <code>State</code>
     */
    public State neighborUpdate(List<State> neighborStates, double probCatch){
        this.setCatchProb(probCatch);
        return neighborUpdate(neighborStates);
    }

    /**
     * Retrieves the probability of catching fire
     * @return probability of catching fire
     */
    public double getCatchProb(){
        return this.catchProb;
    }

    /**
     * Retrieves the random number generator
     * @return random number generator
     */
    protected Random getRandGen(){
        return this.randGen;
    }

    /**
     * Sets the probability of catching fire
     * @param catchProb new probability
     */
    public void setCatchProb(double catchProb){
        this.catchProb = catchProb;
    }

    /*Parent Functions*/

    /**
     * Checks neighboring <code>states</code> and returns the next <code>State</code>. <code>EMPTY</code> is returned
     * by default.
     * @rules: Spreading of Fire
     * @param neighborStates list of the <code>states</code> surrounding the current <code>State</code>
     * @return the next <code>State</code>
     */
    @Override
    public State neighborUpdate(List<State> neighborStates) {
        return EMPTY;
    }

    @Override
    public void setColor(Color stateColor) {
        this.stateColor = stateColor;
    }

    @Override
    public Color getColor() {
        return this.stateColor;
    }

}