// This entire file is part of my masterpiece
// Justin T Wang

/**
 * This is one example of a State enum implementing the State interface.
 *
 * Since the class is forced to override the methods outlined in State, it is now able to extend a feature required by all
 * simulations to better accommodate for the rules of its own.
 *
 * The setter/getter method names are descriptive, with the code itself limited to the fewest lines possible.
 *
 * getTypeFreq utilizes an existing Java feature instead of reimplementing it via a traditional loop structure.
 *
 * The main benefit of using an enum, however, resides in its static behavior. Since its attributes effectively extend across
 * all elements that implement it, changing society-wide rules becomes a one-step process. For instance, instead of having to change
 * the satisfaction rate for every individual cell, a single call to the SegregationState enum will suffice.
 * This adjustment will then be reflected across all of the cells that contain the SegregationState.
 */

package cell.state;

import cell.state.property.StateColor;
import cell.state.property.StateRules;

import javafx.scene.paint.Color;

import java.util.Collections;
import java.util.List;

public enum SegregationState implements State {

    EMPTY(StateColor.SG_EMPTY.getColor()), AGENT_X(StateColor.SG_AGENTX.getColor()), AGENT_O(StateColor.SG_AGENTO.getColor());

    private double satisfactionRate;

    private Color stateColor;

    SegregationState(Color stateColor){
        this.satisfactionRate = StateRules.SG_SATISFACTION.getValue();
        this.setColor(stateColor);
    }

    /*Primary Functions*/

    /**
     * Retrieves the satisfaction rate for this <code>State</code>
     * @return the satisfaction rate (0.0 to 1.0)
     */
    public double getSatisfactionRate(){
        return this.satisfactionRate;
    }

    /**
     * Sets the satisfaction rate for this <code>State</code>
     * @param satisfactionRate new satisfaction rate (0.0 to 1.0)
     */
    public void setSatisfactionRate(double satisfactionRate){
        this.satisfactionRate = satisfactionRate;
    }

    /**
     * Using <code>Collections.frequency</code>, returns the percentage of occurrences of a specified <code>State</code>
     * within a given list
     * @param checkState <code>State</code> to check the occurrences of
     * @param neighborStates List of <code>states</code>
     * @return frequency of <code>checkState</code> within <code>neighborStates</code> as a decimal percentage (0.0 to 1.0)
     */
    protected double getTypeFreq(SegregationState checkState, List<State> neighborStates){
        return ((double) Collections.frequency(neighborStates, checkState))/((double) neighborStates.size());
    }

    /**
     * Indicates whether or not a <code>State</code> is satisfied with the neighbors surrounding it
     * @param neighborStates list of the <code>states</code> surrounding the current <code>State</code>
     * @return true if threshold is met; false otherwise
     */
    public boolean isSatisfied(List<State> neighborStates){
        return (getTypeFreq(this, neighborStates) >= this.satisfactionRate);
    }

    /*Parent Functions*/

    /**
     * Checks neighboring <code>states</code> and returns the next <code>State</code>.
     * @rules: Segregation
     * @param neighborStates list of the <code>states</code> surrounding the current <code>State</code>
     * @return the next <code>State</code>
     */
    @Override
    public State neighborUpdate(List<State> neighborStates) {
        return (isSatisfied(neighborStates)) ? this : EMPTY;
    }

    @Override
    public void setColor(Color stateColor) {
        this.stateColor = stateColor;
    }

    @Override
    public Color getColor() {
        return this.stateColor;
    }

}