package cell;

import java.util.Map;

import cell.state.GameOfLifeState;
import cell.state.SegregationState;
import cell.state.State;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Created by Justin Wang
 */

public class SegregationCell extends Cell{

	
    public SegregationCell(State currentState) {
        super(currentState);
        stateSwapMap.put(SegregationState.AGENT_X, SegregationState.AGENT_O);
        stateSwapMap.put(SegregationState.AGENT_O, SegregationState.EMPTY);
        stateSwapMap.put(SegregationState.EMPTY, SegregationState.AGENT_X);
    }

    public SegregationCell(State currentState, CellShape cellShape) {
        super(currentState, cellShape);
        stateSwapMap.put(SegregationState.AGENT_X, SegregationState.AGENT_O);
        stateSwapMap.put(SegregationState.AGENT_O, SegregationState.EMPTY);
        stateSwapMap.put(SegregationState.EMPTY, SegregationState.AGENT_X);
      
    }
    
}
