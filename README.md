Written by Justin Wang

Project Timeline: *January 26th - February 13th*

Analysis: *February 15th*

------------
### Table of Contents
##### Design Review
1. [Overall Design](#overall-design)
	* [Overview](#overview1)
	* [Features](#features1)
	* [Consistency](#consistency)

------------

<br/>
Accounting for all factors, I allocated a total of ~60 hours towards this project. The workload breaks down as follows:
<br/>

| Time Spent | Activity     						| Date		 |
| :--------- | :----------------------------------  | :--------- |
| 2 Hours	 | Initial Planning Meeting 			| Jan 26	 |
| 2 Hours	 | Second Planning Meeting				| Feb 2		 |
| 4 Hours	 | Outside Planning					    | Cont.		 |
| 8 Hours	 | Implementing States Enums 			| Feb 4		 |
| 4 Hours	 | XML Parser and Properties		    | Feb 5		 |
| 6 Hours	 | Finishing Initial Implementation 	| Feb 6 	 |
| 4 Hours	 | Refactoring States and Properties    | Feb 8		 |
| 8 Hours	 | Implemented Grid Map & Location    	| Feb 10	 |
| 10 Hours	 | Refactored All Code			    	| Feb 11	 |
| 12 Hours	 | Revamped UI and Additional Features  | Feb 12	 |

<br/>
The project was first commissioned on the evening of January 26, and subsequently retired on February 13th. Minor adjustments were made between the final day and the creation of this analysis.
<br/>

# Design Review
<br/>
## Overall Design
<br/>
### Overview
<br/>
The principle design of the project can be broken down into the following components:
<br/>
### Simulation
<br/>
#### `cell` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| Cell | Abstract Class | `updateState`, `swapStates`, `setLocation` | `GameOfLifeCell`, `SegregationCell`, `SpreadingFireCell`, `WatorCell` |
| CellShape | Enum | `getShapeCoordinates`, `getNeighborLocations`, `calculateX`, `calculateY` | N/A |

> Cell Subclasses:  `GameOfLifeCell`, `SegregationCell`, `SpreadingFireCell`, `WatorCell`

<br />

#### `cell.location` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| Location | Class | `getRow`, `getCol`, `compareTo`, `equals`, `hashCode` | N/A |

<br />

#### `cell.state` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| State | Interface | `neighborUpdate`, `getColor`, `setColor` | `GameOfLifeState`, `SegregationState`, `SpreadingFireState`, `WatorState` |

> State Subclasses:  `GameOfLifeState`, `SegregationState`, `SpreadingFireState`, `WatorState`

<br />

#### `society` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| Society | Abstract Class | `updateNextState`, `advanceNextState` | `GameOfLifeSociety`, `SegregationSociety`, `SpreadingFireSociety`, `WatorSociety` |
| Grid | Class | `setCell`, `getNeighborCellList`, `getNeighborCellMap`, `getGridLocations`, `renderCells` | N/A |

> Society Subclasses:  `GameOfLifeSociety`, `SegregationSociety`, `SpreadingFireSociety`, `WatorSociety`

<br />

--------------
### User Interface | Visuals

#### `ui` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| Main | Main Class | `main`, `start` | N/A |
| UserInterfaceX | Class | `createSociety`, `getSettings`, `renderSociety` | N/A |
| Animator | Class | `getLineChart`, `initialize`, `nextStep`  | N/A |
| Chart | Class | `getChart`, `updateChart` | N/A |


<br />

-----------------
### Auxiliary | Properties

#### `xml` Package 
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| XMLParser | Class | `initDoc`, `getSocietySetting` | N/A |
| XMLTag | Enum | `getTag` | N/A |

<br />

#### `cell.state.property` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| StateColor | Enum | `getColor` | N/A |
| StateRules | Enum | `getValue` | N/A |

<br />

#### `society.property` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| SocietySetting | Class | `getCellList`, `getRow`, `getCol` | N/A |
| SocietyType | Enum | `getNameOfSociety`, `createSociety`, 'fromString' | `GameOfLifeSociety`, `SegregationSociety`, `SpreadingFireSociety`, `WatorSociety` |

<br />

#### `ui.property` Package
| Class					 | Type					 | Notable Methods					 | Subclasses					 |
| :--------------------- | :-------------------- | :-------------------------------- | :------------------------------ |
| UITags | .properties | N/A | N/A |

<br />

-------------------------
<br/>
### *Visuals*
<br/>
#### Main
<br/>
The main class is primarily responsible for presenting an instance of the UserInterfaceX. It extends the Application class of JavaFX.
<br/>
#### UserInterfaceX:
<br/>
This class primarily serves hosts to the various Society instances (depending on the user's selection). It also houses all of the GUI components that will accept and process user input.
<br/>
Upon initialization, the UI generates all GUI elements, but only enables the *Select File* button and *Select A Simulation* combo-box to allow for user input.
<br />
<br />
![initScreen](img/initScreen.png)
<br />
<br />
Once the user selects a simulation, an instance of that Society is rendered to the scrollable pane (right panel). At any given moment in-between animations, the user will be granted an opportunity to adjust simulation parameters. 
<br/>
The simulation is initiated when the user presses the *Start | Stop* button at the bottom-left side of the screen.
<br/>
When a simulation is open, the user is granted access to all relevant settings buttons. Any animation-specific adjustments will be relayed to the Animation object that controls each step of the overarching simulation, while any simulation-specific adjustments made in the *Settings* tab will be passed down to the underlying State and Society infrastructures.
<br/>
At all times the UI is responsible for displaying the current state of the Society with respect to the ongoing simulation. The placement of GUI elements is outlined as follows:
<br/>
* The bottom toolbar is responsible for housing all animation settings elements
	* Animation Rate: Adjusts the speed of the animation (i.e. how fast it progresses through steps)
	* Cell Size: Ability to resize the grid of cells during any moment in the simulation
	* Select A Shape: Changes the grid of cells to a specified shape (Square is generated as default)
	* Next Turn: If the simulation is paused, the user is allowed to progress the simulation on a turn-by-turn basis
	* Start | Stop: Starts and stops the simulation. The simulation is 'stopped' by default.
* The left pane houses two tabs: *Settings* and *Graph*
	* Settings: Houses all simulation-specific sliders
	* Graph: Presents a graph of Cell populations in realtime
* The top toolbar houses simulation selection elements
	* Select A File: Opens a file dialog that allows the user to select an appropriate .xml game file
	* Select A Simulation: Allows user to select a simulation from a predetermined list
* The center/right pane houses the Society (houses the Grid of cells in a scrollable pane)
<br/>

<br/>
<br/>

![initScreen](img/mainScreen.png)

<br/>
<br/>

#### Animation & Chart:
<br/>
Animation serves to spearhead the progression of steps in a simulation. Working in tandem with Society, it progresses all visual/graphical information and sends it to the UI where it will then be processed and displayed to the user. It generates *KeyFrames* for each step of an animation that a *Timeline* then processes. The speed of the animation is also handled by this class.
<br/>
As the name implies, Chart serves to plot society population information onto a line graph. The Animation class is responsible for generating a Chart per Society that is relayed to the UI.
<br/>

-------------------
### *Simulation*
<br/>
#### Society:
<br/>
Society serves the purpose of initializing game rules/specefications depending on what simulation was chosen.
As previously mentioned, an instance of the Society class will be generated upon commissioning the main UserInterfaceX. Using a list of Cells extracted from an XML file, it will work to generate a Grid that will serve as the main infrastructure of the Society. 
<br/>
As the organizational platform for the primary hierarchical elements - namely Grid, Cells, and States. it is responsible for iterating through all Cells when called upon by the Animation engine (where the step-by-step progression of the simulation is maintained).
<br/>
Society's main feature revolves around the ability to parse through all Cells, update their States based on certain parameters, and update said Cells with their updated States. Certain Society types (like WatorSociety), contain special helper methods that help them keep track of simulation-specific parameters.
<br/>
#### Grid:
<br/>
Grid is responsible for providing a data structure to the collection of Cells for a given Society. In our case, a *HashMap* is implemented to store all of the cells via a Location key. In order to differentiate cells based on their grid position, the Location class was created such that a unique *HashCode* is generated via a Cell's x and y coordinates. Grid also has the ability to locate a list of Cells based off of their type & proximity to a given Location and can also set certain properties of the Cell such as shape and size. All pixel coordinate and rendering responsibilities are also handled by this class (i.e. generates pixel-based coordinates using the CellShape's coordinate generating function). 
<br/>
When isolating groups of cells, it extracts grid-specific Locations from the CellShape class. This information is then used to collect all cells with those Locations and pass them along to the Cell, where neighbor and next state information are extrapolated.
<br/>
#### Cell:
<br/>
At the heart of the visual/graphical interface lies the Cell, whose purpose is twofold: host data/information pertaining to an instance of a CA (cellular automata) and present it graphically via a *Node* object (e.g. *Polygon*). In terms of the latter, the Cell extends the *Polygon* class to allow for visually representation of the cell on the UI. The properties of *Polygon* (i.e. Color, Coordinates, Size) will change when any of the Cell's data components are altered by the user.
<br/>
All identification responsibilities are conferred to a State object. Information that is extracted from the State is then directly translated to visual changes via the Cell. For instance, if at any given time the State of the Cell were to change its type from *Tree* to *Burning*, the Cell would extrapolate said change from State and communicate the adjustment to the UI (namely the change of color from GREEN to RED) via a selection of helper methods.
<br/>
#### State:
<br/>
Arguably the most fundamental 'building block' of the game, State serves to contain the 'characterizations' of a Cell. At the center of this class sits a neighborUpdate method. This allows the State of a Cell to change when subject to changing neighbors. In this context, the State is how a Cell differentiates itself within an environment that it is enclosed in at any given time. This includes information that pertain to the Cell's neighboring-entities, as well as simulation rules and parameters (i.e. alive-cell check for GameOfLife). When called upon by the Society class, a Cell will use its instance of State to query its current status and update said status. 
<br/>
In order to preserve expandability, State grants the cell game-type specificity. Any structural adjustments will also be reflected by the State. In other words, State serves the overarching purpose of enforcing the specific game type outlined by the Society. 
<br/>
#### CellShape:
At any given time, the CellShape class maintains the ability to check the host's immediate spatial vicinity and identify the neighboring Cells of a given host Cell (with help from Location). This information is then used whenever an update function is called to determine the next state of the Cell.
<br/>
#### Location:
The Location class simply holds the grid-specific coordinates of any given cell. Having extended Comparable<>, Location also serves as the primary key for a Cell within a Grid. It also ensures that Location objects are solely identified by their x and y coordinates. This means that any Location object instance can be directly compared against one another (i.e. a new instance of the Location (0,0) will be the equivalent to another separate instance of the same Location).
<br/>
### *Configuration*
<br/>

#### XMLParser:
XML Parser contains all relevant parsing elements and attributes, as well as an instance of a SocietySetting object. When it is first called upon, it receives details regarding the xml file location and generates a SocietySetting specific to the parsed game file. In essence, this class is exclusively responsible for picking and reading the right configuration file. From the SocietySetting object, XMLParser is able to relay game rules and initialization parameters back to the UI class.
<br/>
In general, error handling will be orchestrated by an error-handling class (`EHandler`) that serve as a checkpoint between any file-parsing related calls.
<br/>
#### Properties Classes, Enums, and Files
> Includes: XMLTag, StateColor, StateRules, SocietySetting, UITags

<br/>
`StateColor`: Contains the standard color information of all Cell types.
`StateRules`: Contains 'magic number' variables for all simulation types.
`XMLTag`: Contains the mapping of all XML tag names (for file parsing purposes)
`SocietySetting`: Class that holds all parsed information regarding a Society
`UITags`: Contains miscellaneous parameters (i.e. UI boundaries, Society scaling, Names for UI elements, etc.)
<br/>
### *Adding A New Simulation*
<br/>
For a new simulation to be added, the following files must be implemented/generated:

* A `State` Enum (extends the `State` Interface)
* A `Cell` Class (extends the `Cell` Abstract Class)
* A `Society` Class (extends the `Society` Abstract Class)

These new additions might require additions to the Properties Classes, Enums, and Files (listed above). The UserInterfaceX class would also need to be updated in order to add simulation-specific GUI elements.
<br/>
### *Dependencies*
<br/>
Most of the dependencies are relatively clear and easy to find, with most being handled through dependency injections. Take, for instance, the following constructor headers:
<br/>
```java
public Grid(int row, int col, List<Cell> cellList, CellShape cellShape){...}
public Society(int row, int col, List<Cell> cellList, CellShape cellShape){...}
public Cell(State currentState, CellShape cellShape){...}
```
<br/>
These are some of the examples of dependency injection that exist throughout the project.
<br/>
In terms of subclass type requirements, all simulation-identification needs are handled by the various collections of enums (for Society, States, and other parameters). For instance, for methods that require a State or Cell as a parameter, they use an interface/parent class mask to eliminate backdoor dependencies. For instance, the method
<br/>
```java
public void setCell(Location cellLocation, Cell newCell){...}
```
<br/>
in Grid does not require a specific sub-class as its paramter. Instead, it allows for any Cell object to be added to the Map structure.
<br/>
Method and field dependencies are all public, as evidenced by the various get/set methods that exist across multiple levels of the hierarchy. Global variables are never directly accessed by another class other than the owner class. 
<br/>